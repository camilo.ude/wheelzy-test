﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wheelzy.Domain.Entities
{
    public class BuyOffer
    {
        public int BuyOfferId { get; set; }
        public int CarId { get; set; }
        public Car Car { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public double Amount { get; set; }
        public bool IsCurrent { get; set; }
        public DateTime CreatedDate { get; set; }

        public ICollection<StatusHistory> StatusHistories { get; set; }

    }
}
