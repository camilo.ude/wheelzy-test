﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wheelzy.Domain.Entities;

namespace Wheelzy.Domain.Response
{
    public class BuyOfferDetailResponse
    {
        public int CarId { get; set; }      
        public int CarYear { get; set; }
        public string CarMake { get; set; }
        public string CarModel { get; set; }
        public string CarSubModel { get; set; }
        public string CarZipCode { get; set; }
        public string BuyerName { get; set; }
        public double Amount { get; set; }
        public string StatusName { get; set; }
        public DateTime? StatusDate { get; set; }

    }
}
