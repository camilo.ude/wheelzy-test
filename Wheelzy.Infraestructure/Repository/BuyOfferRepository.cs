﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Wheelzy.Domain.Entities;
using Wheelzy.Domain.Repository;
using Wheelzy.Domain.Response;

namespace Wheelzy.Infraestructure.Repository
{
    public class BuyOfferRepository : IBuyOfferRepository
    {
        private readonly WheelzyContext _context;
        public BuyOfferRepository(WheelzyContext wheelzyContext)
        {
            _context = wheelzyContext;
        }

        /*Technical Assessment*/

        #region Point 1
        /*
          Write a SQL query to show the car information, current buyer name with its quote and
          current status name with its status date. Do the same thing using Entity Framework.
          Make sure your queries don’t have any unnecessary data.
         */
        public async Task<IEnumerable<BuyOfferDetailResponse>> GetBuyOffers()
        {
            var query = (from offer in _context.BuyOffers
                         join status in _context.StatusHistories on offer.BuyOfferId equals status.BuyOfferId
                         where status.ISCurrentStatus
                         select new BuyOfferDetailResponse
                         {
                             CarId = offer.CarId,
                             CarMake = offer.Car.CarMake,
                             CarModel = offer.Car.CarModel,
                             CarSubModel = offer.Car.CarSubModel,
                             CarYear = offer.Car.CarYear,
                             CarZipCode = offer.Car.ZipCode.Code,
                             BuyerName = offer.User.UserName,
                             Amount = offer.Amount,
                             StatusName = status.Status.StatusName,
                             StatusDate = status.CreatedDate
                         });

            return await query.ToListAsync();
        }

        public async Task<IEnumerable<BuyOfferDetailResponse>> GetBuyOffersSQL()
        {

            string query = @"SELECT 
                            b.CarId, 
                            c.CarMake, 
                            c.CarModel, 
                            c.CarSubModel, 
                            c.CarYear, 
                            z.Code AS CarZipCode, 
                            u.UserName AS BuyerName, 
                            b.Amount, 
                            st.StatusName, 
                            s.CreatedDate AS StatusDate
                            FROM BuyOffers AS b
                            INNER JOIN StatusHistories AS s ON b.BuyOfferId = s.BuyOfferId
                            INNER JOIN Cars AS c ON b.CarId = c.CarId
                            INNER JOIN ZipCode AS z ON c.ZipCodeId = z.ZipCodeId
                            INNER JOIN Users AS u ON b.UserId = u.UserId
                            INNER JOIN Statuses AS st ON s.StatusId = st.StatusId
                            WHERE s.ISCurrentStatus = 1";

            var result = _context.Database.SqlQueryRaw<BuyOfferDetailResponse>(query).ToList();

            return result;
        }

        #endregion Point 1

        #region Point 2

        /*What would you do if you had data that doesn’t change often but it’s used pretty much all the time?*/

        ///R. I would implement a cache service to store that data

        #endregion Point 2

        #region Point 3       
        /*
        public void UpdateCustomersBalanceByInvoices(List<Invoice> invoices)
        {
            var customers = dbContext.Customers.Where(customer => invoices.Select(invoice => invoice.CustomerId).ToArray().Contains(customer.CustomerId)).ToList();

            foreach (var customer in customers)
            {
                customer.Balance -= invoices.FirstOrDefault(x => x.CustomerId = customer.CustomerId).Total;
            }

            dbContext.SaveChanges();
        }*/


        #endregion  Point 3


        #region Point 4

        public async Task<IEnumerable<OrderDTO>> GetOrders(DateTime dateFrom, DateTime dateTo, List<int> customerIds, List<int> statusIds, bool? isActive)
        {
            //I  adapted the requirement to my model 
            var query = _context.BuyOffers.Where(x =>
            (x.CreatedDate >= dateFrom && x.CreatedDate <= dateTo)
            && (customerIds.Any() ? customerIds.Contains(x.UserId) : true)
            && (statusIds.Any() ? x.StatusHistories.Any(status => statusIds.Contains(status.StatusId)) : true)
            && (x.IsCurrent == (isActive == null ? x.IsCurrent : isActive)))
                .Select(x => new OrderDTO
                {
                    BuyOfferId = x.BuyOfferId,
                    IsCurrent = x.IsCurrent,
                    Amount = x.Amount,
                    CreatedDate = x.CreatedDate
                });


            return await query.ToListAsync();

        }
        #endregion Point 4

        #region Point 5

        /*
            Bill, from the QA Department, assigned you a high priority task indicating there’s a bug
            when someone changes the status from “Accepted” to “Picked Up”.
            Define how you would proceed, step by step, until you create the Pull Request.
         */

        //// I would proceed by the next way
        //// 1. I would try to replicate the issue in the environment that was reported
        //// 2. If I can replicate the issue then I would try to replicate it on dev and local environment to discard missing code
        //// 3. If I can replicate on dev or local, I would proceed to create a new branch wich will be named with as the Ticket reported
        //// 4. Debug start to debug, find the issue  and fix it
        //// 5. Make some test to ensure that the issue was fixed
        //// 6. Do the commit and push the changes
        //// 7. Create the pull request
        //// 8. Await for the deployment to develop and test again
        //// 9. Set the ticket as done
        //// * If in the step 1 I'm not able to replicate the issue, I would ask to Bill wich were the steps 
       
       
        #endregion Point 5
    }



}
