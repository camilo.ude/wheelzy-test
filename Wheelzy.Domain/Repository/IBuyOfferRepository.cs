﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wheelzy.Domain.Response;

namespace Wheelzy.Domain.Repository
{
    public interface IBuyOfferRepository
    {
        Task<IEnumerable<BuyOfferDetailResponse>> GetBuyOffers();
        Task<IEnumerable<BuyOfferDetailResponse>> GetBuyOffersSQL();
        Task<IEnumerable<OrderDTO>> GetOrders(DateTime dateFrom, DateTime dateTo, List<int> customerIds, List<int> statusIds, bool? isActive);
    }
}
