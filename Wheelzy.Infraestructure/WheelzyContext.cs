﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wheelzy.Domain.Entities;

namespace Wheelzy.Infraestructure
{
    public class WheelzyContext:DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<BuyOffer> BuyOffers { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<StatusHistory> StatusHistories { get; set; }

        public WheelzyContext(DbContextOptions<WheelzyContext> options)
          : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StatusHistory>().HasOne(x => x.PreviousStatus).WithMany().HasForeignKey(x=>x.PreviousStatusId);
            modelBuilder.Entity<BuyOffer>().HasOne(x => x.User).WithMany().HasForeignKey(x=>x.UserId).OnDelete(DeleteBehavior.NoAction);

            base.OnModelCreating(modelBuilder);
        }
    }
}
