﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wheelzy.Domain.Entities
{
    public class ZipCode
    {
        public int ZipCodeId { get; set; }
        public string Code { get; set; }
    }
}
