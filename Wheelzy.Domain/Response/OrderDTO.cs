﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wheelzy.Domain.Response
{
    public class OrderDTO
    {
        public int BuyOfferId { get; set; }
        public double Amount { get; set; }
        public bool IsCurrent { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
