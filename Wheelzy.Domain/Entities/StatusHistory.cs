﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wheelzy.Domain.Entities
{
    public class StatusHistory
    {
        public int StatusHistoryId { get; set; }
        public int BuyOfferId { get; set; }
        public BuyOffer BuyOffer { get; set; }
        public int StatusId { get; set; }
        public Status Status { get; set; }
        public int? PreviousStatusId { get; set; }
        public Status PreviousStatus { get; set; }
        public string StatusDetail { get; set; }
        public string Creator { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool ISCurrentStatus { get; set; }

    }
}
