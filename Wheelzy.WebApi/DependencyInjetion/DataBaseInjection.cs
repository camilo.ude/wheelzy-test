﻿using Microsoft.EntityFrameworkCore;
using Wheelzy.Infraestructure;

namespace Wheelzy.WebApi.DependencyInjetion
{
    public static class DataBaseInjection
    {
        public static IServiceCollection ConfigDatabase(this IServiceCollection services, ConfigurationManager configuration)
        {

            string connectionString = configuration.GetConnectionString("wheelzydb");
            services.AddDbContext<WheelzyContext>(option => {
                option.UseSqlServer(connectionString, b => b.MigrationsAssembly("Wheelzy.WebApi"));

            });
            return services;
        }
    }
}
