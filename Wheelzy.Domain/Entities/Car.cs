﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wheelzy.Domain.Entities
{
    public class Car
    {
        public int CarId { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int CarYear { get; set; }
        public string CarMake { get; set; }
        public string CarModel { get; set; }
        public string CarSubModel { get; set; }
        public int ZipCodeId { get; set; }
        public ZipCode ZipCode { get; set; }
    }
}
